# Exemple de Portfolio

> Apprendre à montrer ses compétences et projets aux autres

Le portfolio est généré avec l'outil [MkDocs](https://mkdocs.org)
et hébergé automatiquement avec [GitLab Pages](https://docs.gitlab.com/ce/user/project/pages/).

## Installation

```bash
pipenv install -d
pipenv shell
mkdocs serve
```

Ouvrez un navigateur à l'URL [http://localhost:8000](http://localhost:8000).

## Contenu

Le contenu de la base de connaissance se trouve dans le dossier [portfolio](./portfolio) :

- [index.md](./portfolio/index.md) : présentation générale

Toutes les pages du portfolio doivent être rédigées avec le langage [Markdown](https://www.markdownguide.org).

## Génération

Pour générer le site web statique :

```bash
mkdocs build
```

Le site web prêt à être déployé est généré dans le dossier [site](./site).
